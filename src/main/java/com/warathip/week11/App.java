package com.warathip.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Bat bat1 = new Bat("Batman");
       bat1.eat();
       bat1.sleep();
       bat1.fly();
       bat1.landing();
       bat1.takeoff();

       Fish fish1 = new Fish("jijee");
       fish1.eat();
       fish1.sleep();
       fish1.swim();

       Plane plane1 = new Plane("Nok Air","NokAir Emgine");
       plane1.fly();
       plane1.landing();
       plane1.takeoff();

       Snake snake1 = new Snake("kiki");
       snake1.crawl();
       snake1.swim();
       snake1.eat();
       snake1.sleep();

       Crocodile crocodile1 = new Crocodile("pinky", 4);
       crocodile1.crawl();
       crocodile1.swim();
       crocodile1.eat();
       crocodile1.sleep();

       Submarine submarine1 = new Submarine("Matchanu","Matchanu engine");
       submarine1.swim();

       Bird bird1 = new Bird("Nokky",2);
       bird1.eat();
       bird1.sleep();
       bird1.fly();
       bird1.landing();
       bird1.takeoff();

       Cat cat1 = new Cat("Kitty", 4);
       cat1.eat();
       cat1.sleep();
       cat1.walk();

       Dog dog1 = new Dog("Doggy", 4);
       dog1.eat();
       dog1.sleep();
       dog1.walk();

       Rat rat1 = new Rat("Mikky", 4);
       rat1.eat();
       rat1.sleep();
       rat1.walk();

       Human human1 = new Human("Warathip", 2);
       human1.eat();
       human1.sleep();
       human1.walk();
       human1.swim();

       Flyable[] flyablesObjects = {bat1, plane1,bird1};
       for(int i=0; i<flyablesObjects.length;i++) {
           flyablesObjects[i].fly();
           flyablesObjects[i].landing();
           flyablesObjects[i].takeoff();
       }
       Swimable[] swimablesObjects = {crocodile1,fish1,snake1,human1,submarine1};
       for(int i=0; i<swimablesObjects.length; i++) {
           swimablesObjects[i].swim();
       }
       Crawlable[] crawlablesObjects = {crocodile1,snake1};
       for(int i=0; i<crawlablesObjects.length; i++) {
           crawlablesObjects[i].crawl();
       }
       Walkable[] walkablesObjects = {cat1,dog1,human1,rat1};
       for(int i=0; i<walkablesObjects.length; i++) {
           walkablesObjects[i].walk();
       }
    }
}
