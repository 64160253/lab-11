package com.warathip.week11;

public class Human extends Animal implements Walkable,Swimable {

    public Human(String name, int numberOfLeg) {
        super(name,2);
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim ");
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " walk ");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat ");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep ");
    }
    @Override
    public String toString() {
        return "Human(" + this.getName() + ")";
    }
    
}
