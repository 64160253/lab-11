package com.warathip.week11;

import javax.swing.ImageIcon;

public class Dog  extends Animal implements Walkable{

    public Dog(String name, int numberOfLeg) {
        super(name, 4);
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " walk ");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat ");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep ");
    }
    @Override
    public String toString() {
        return "Dog(" + this.getName() + ")";
    }
}
